﻿using EWayBill.LogitaxAPIForStandard.CommonService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.EWayBill
{
    public class EWayBill
    {
        HTTPMethods hTTPMethods;
        string URL = string.Empty;
        public EWayBill()
        {
            hTTPMethods = new HTTPMethods();
        }

        public object GenerateJSONEWB(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EwayBillASP/GenerateJSONEWB";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EwayBillASP/GenerateJSONEWB";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }

        public object CancelEWB(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EwayBillASP/CancelEWB";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EwayBillASP/CancelEWB";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }

        public object RejectEWB(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EwayBillASP/RejectEWB";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EwayBillASP/RejectEWB";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }

        public object ConsolidateEWB(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EwayBillASP/ConsolidateEWB";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EwayBillASP/ConsolidateEWB";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }

        public object UpdateVehicle(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EwayBillASP/UpdateVehicle";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EwayBillASP/UpdateVehicle";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }

        public object UpdateTransporter(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EwayBillASP/UpdateTransporter";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EwayBillASP/UpdateTransporter";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }

        public object ExtendValidity(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EwayBillASP/ExtendValidity";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EwayBillASP/ExtendValidity";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }

        public object RegenerateConsolidateEWB(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EwayBillASP/RegenerateConsolidateEWB";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EwayBillASP/RegenerateConsolidateEWB";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }
    }
}
