﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.CommonService
{
    public class HTTPMethods
    {
        public string HTTPPostMethod(string request, string URL, header_object_value Session_Object_Value)
        {
            SecurityProtocolType origSecurityProtocol = System.Net.ServicePointManager.SecurityProtocol;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest httprequest = (HttpWebRequest)WebRequest.Create(URL);
            httprequest.Method = "POST";
            httprequest.KeepAlive = true;
            httprequest.AllowAutoRedirect = false;
            httprequest.Accept = "*/*";
            httprequest.ContentType = "application/json";
            httprequest.Timeout = 300000;
            httprequest.Headers.Add("CLIENTCODE", Session_Object_Value.CLIENTCODE);
            httprequest.Headers.Add("USERCODE", Session_Object_Value.USERCODE);
            httprequest.Headers.Add("PASSWORD", Session_Object_Value.PASSWORD);
            httprequest.Headers.Add("GSTIN", Session_Object_Value.GSTIN);
            httprequest.ContentLength = request.Length;
            httprequest.SendChunked = true;
            using (StreamWriter streamWriter = new StreamWriter(httprequest.GetRequestStream()))
            {
                streamWriter.Write(request);
                streamWriter.Flush();
                streamWriter.Close();
            }
            WebResponse httpresponse = httprequest.GetResponse();
            string response = new StreamReader(httpresponse.GetResponseStream()).ReadToEnd();
            return response;
        }

        public string HTTPPostMethodWithoutBody(string URL, header_object_value Session_Object_Value)
        {
            SecurityProtocolType origSecurityProtocol = System.Net.ServicePointManager.SecurityProtocol;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest httprequest = (HttpWebRequest)WebRequest.Create(URL);
            httprequest.Method = "POST";
            httprequest.KeepAlive = true;
            httprequest.AllowAutoRedirect = false;
            httprequest.Accept = "*/*";
            httprequest.ContentType = "application/json";
            httprequest.Timeout = 300000;
            httprequest.Headers.Add("CLIENTCODE", Session_Object_Value.CLIENTCODE);
            httprequest.Headers.Add("USERCODE", Session_Object_Value.USERCODE);
            httprequest.Headers.Add("PASSWORD", Session_Object_Value.PASSWORD);
            httprequest.Headers.Add("GSTIN", Session_Object_Value.GSTIN);
            httprequest.SendChunked = true;
            WebResponse httpresponse = httprequest.GetResponse();
            string response = new StreamReader(httpresponse.GetResponseStream()).ReadToEnd();
            return response;
        }

        public string HTTPGetMethod(string request, string URL, header_object_value Session_Object_Value)
        {
            SecurityProtocolType origSecurityProtocol = System.Net.ServicePointManager.SecurityProtocol;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest httprequest = (HttpWebRequest)WebRequest.Create(URL);
            httprequest.Method = "Get";
            httprequest.KeepAlive = true;
            httprequest.AllowAutoRedirect = false;
            httprequest.Accept = "*/*";
            httprequest.ContentType = "application/json";
            httprequest.Timeout = 300000;
            httprequest.Headers.Add("CLIENTCODE", Session_Object_Value.CLIENTCODE);
            httprequest.Headers.Add("USERCODE", Session_Object_Value.USERCODE);
            httprequest.Headers.Add("PASSWORD", Session_Object_Value.PASSWORD);
            httprequest.Headers.Add("GSTIN", Session_Object_Value.GSTIN);
            httprequest.ContentLength = request.Length;
            httprequest.SendChunked = true;
            WebResponse httpresponse = httprequest.GetResponse();
            string response = new StreamReader(httpresponse.GetResponseStream()).ReadToEnd();
            return response;
        }
    }
}
