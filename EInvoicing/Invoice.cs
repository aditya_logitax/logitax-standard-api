﻿using EWayBill.LogitaxAPIForStandard.CommonService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.EInvoicing
{
    public class Invoice
    {
        HTTPMethods hTTPMethods;
        string URL = string.Empty;
        public Invoice()
        {
            hTTPMethods = new HTTPMethods();
        }

        public object GenerateJSONEInvoice(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EInvoiceASP/GenerateJSONEinvoice";
            }

            else if(APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EInvoiceASP/GenerateJSONEinvoice";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }

        public object CancelEinvoice(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EInvoiceASP/CancelEinvoice";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EInvoiceASP/CancelEinvoice";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }


        public object CancelEWBInvoice(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EInvoiceASP/CancelEWBInvoice";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EInvoiceASP/CancelEWBInvoice";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }

        public object GenerateEWBEinvoice(object content, string APIUrlType, header_object_value header_Object_Value)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/aspUAT/api/EInvoiceASP/GenerateEWBEinvoice";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/asp/api/EInvoiceASP/GenerateEWBEinvoice";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL, header_Object_Value);
            var resoponsejson = JsonConvert.DeserializeObject<object>(resoponse);
            return resoponsejson;
        }
    }
}
