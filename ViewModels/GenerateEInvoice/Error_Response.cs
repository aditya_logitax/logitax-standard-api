﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.ViewModels.GenerateEInvoice
{
    public class Error_Response
    {
        public string status { get; set; }
        public object Data { get; set; }
        public object ErrorDetails { get; set; }
        public object InfoDtls { get; set; }
    }
}
