﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.ViewModels.CancelEWayBillModel
{
    public class cancel_eway_bill_response
    {
        public long ewayBillNo { get; set; }
        public string cancelDate { get; set; }
    }
}
