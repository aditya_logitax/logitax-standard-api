﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.ViewModels.CancelEWayBillModel
{
    public class error_response
    {
        public string status { get; set; }
        public Error error { get; set; }
    }

    public class Error
    {
        public int errorCodes { get; set; }
    }
}
