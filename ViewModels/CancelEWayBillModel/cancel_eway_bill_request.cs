﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.ViewModels.CancelEWayBillModel
{
    public class cancel_eway_bill_request
    {
        public long ewbNo { get; set; }
        public int cancelRsnCode { get; set; }
        public string cancelRmrk { get; set; }
    }
}
