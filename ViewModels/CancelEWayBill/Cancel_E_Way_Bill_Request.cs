﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.ViewModels.CancelEWayBill
{
    public class Cancel_E_Way_Bill_Request
    {
        public long ewbNo { get; set; }
        public int cancelRsnCode { get; set; }
        public string cancelRmrk { get; set; }
    }
}
