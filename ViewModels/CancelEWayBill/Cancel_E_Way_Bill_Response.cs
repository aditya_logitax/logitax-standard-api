﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.ViewModels.CancelEWayBill
{
    public class Cancel_E_Way_Bill_Response
    {
        public string ewayBillNo { get; set; }
        public string cancelDate { get; set; }
    }
}
