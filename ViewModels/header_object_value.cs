﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWayBill.LogitaxAPIForStandard
{
    public class header_object_value
    {
        public string CLIENTCODE { get; set; }
        public string USERCODE { get; set; }
        public string PASSWORD { get; set; }
        public string GSTIN { get; set; }
    }
}