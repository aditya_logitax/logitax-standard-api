﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.ViewModels.CancelEInvoice
{
    public class Cancel_E_Invoice_Response
    {
        public string status { get; set; }
        public object data { get; set; }
        public object ErrorDetails { get; set; }
        public object InfoDtls { get; set; }
    }
}
