﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.ViewModels.GenerateEWayBill
{
    public class EWay_Bill_Response
    {
        public long ewayBillNo { get; set; }
        public string ewayBillDate { get; set; }
        public string validUpto { get; set; }
        public string alert { get; set; }
    }
}
