﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EWayBill.LogitaxAPIForStandard.ViewModels.GenerateEWayBill
{
    public class Error_Response
    {
        public string status { get; set; }
        public Error error { get; set; }
        public object info { get; set; }
    }

    public class Error
    {
        public int errorCodes { get; set; }
    }

}
